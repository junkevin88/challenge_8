package com.javainuse.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringCloudConfig {

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/api/film/**")
                        .uri("http://localhost:8082/")
                        .id("filmModule"))

                .route(r -> r.path("/api/schedule/**")
                        .uri("http://localhost:8083/")
                        .id("scheduleModule"))

                .route(r -> r.path("/api/consumer/**")
                        .uri("http://localhost:8084/")
                        .id("seatModule"))
                .build();
    }

}