package com.binar.microservice.film.service.impl;

import com.binar.microservice.film.entity.Films;
import com.binar.microservice.film.repository.FilmRepository;
import com.binar.microservice.film.service.FilmService;
import com.binar.microservice.film.utils.Config;
import com.binar.microservice.film.utils.Response;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.Map;

@Service
public class FilmImpl implements FilmService {


    private static Logger logger = LoggerFactory.getLogger(com.binar.microservice.film.service.impl.FilmImpl.class);
    @Autowired
    public FilmRepository filmRepository;

    @Autowired
    public Response response;

    @Autowired
    public RestTemplateBuilder restTemplateBuilder;


    @Override
    public Map save(Films request) {
        try {
            if (request.getName() == null) {
                return response.error("Name is required!", Config.ERROR_401);
            }
            Films doSave = filmRepository.save(request);
            return response.sukses(doSave);
        } catch (Exception e) {
            logger.error("Error save, {} " + e);
            return response.error("Error save: " + e.getMessage(), Config.ERROR_500);
        }
    }

    @Override
    public Map update(Films request) {
        try {
            if (request.getId() == null) {
                return response.error("Id is required!", Config.ERROR_401);
            }
            Films checkingData = filmRepository.getById(request.getId());
            if (checkingData == null) {
                return response.error("Data cannot be found!", Config.ERROR_404);
            }

            checkingData.setStatus(request.getStatus());
            checkingData.setName(request.getName());
            Films doSave = filmRepository.save(checkingData);
            return response.sukses(doSave);

        } catch (Exception e) {
            logger.error("Error update, {} " + e);
            return response.error("Error update: " + e.getMessage(), Config.ERROR_500);
        }
    }

    @Override
    public Map delete(Long id) {
        try {
            if (id == null) {
                return response.error("Id is required!", Config.ERROR_401);
            }
            Films checkingData = filmRepository.getById(id);
            if (checkingData == null) {
                return response.error("Data cannot be found!", Config.ERROR_404);
            }

            checkingData.setDeleted_date(new Date());
            Films saveDeleted = filmRepository.save(checkingData);
            return response.sukses("sukses");
        } catch (Exception e) {
            logger.error("Error delete, {} " + e);
            return response.error("Error delete: " + e.getMessage(), Config.ERROR_500);
        }
    }

    @Override
    public Map getById(Long request) {
        try {
            if (request == null) {
                return response.error("Id is required!", Config.ERROR_401);
            }
            Films checkingData = filmRepository.getById(request);
            if (checkingData == null) {
                return response.error("Data cannot be found!", Config.ERROR_404);
            }
            return response.sukses(checkingData);

        } catch (Exception e) {
            logger.error("Error get by id, {} " + e);
            return response.error("Error get by id: " + e.getMessage(), Config.ERROR_500);
        }

    }

    @Override
    public Iterable<Films> getFilmsFilterStatus() {
        return filmRepository.getFilmsFilterStatus();
    }

    @Override
    public Map getPriceInSchedule(Long id) {


        try {

            if (id == null) {
                return response.error("Id is required!", Config.ERROR_401);
            }
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", "*/*");
            headers.set("Content-Type", "application/json");
            String request1 = "";
            // request Body
            HttpEntity<String> entity = new HttpEntity<String>(request1, headers);
            String url = "http://localhost:8080/api/schedule/get/" + id;

            ResponseEntity<JsonNode> exchange = restTemplateBuilder.build().exchange(url, HttpMethod.GET, null, JsonNode.class);



            String data = String.valueOf(exchange.getBody());

            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(data);

            JsonNode jsonPrice = jsonNode.get("data");


//            ObjectMapper objectMapper = new ObjectMapper();

            return response.sukses(jsonPrice.get("price"));

        } catch (Exception e) {
            logger.error("Error get by id, {} " + e);
            return response.error("Error get by id: " + e.getMessage(), Config.ERROR_500);
        }


//    @Override
//    public Map getFilmsFilterStatus(Long request) {
//        try {
//
//            List<Films> checkingData = filmRepository.getFilmsFilterStatus(request);
//            if (checkingData == null) {
//                return response.error("Data cannot be found!", Config.ERROR_404);
//            }
//            return response.sukses(checkingData);
//
//        } catch (Exception e) {
//            logger.error("Error get by id, {} " + e);
//            return response.error("Error get by id: " + e.getMessage(), Config.ERROR_500);
//        }
//    }

    }

}
