package com.binar.microservice.film.entity;


import com.binar.microservice.film.entity.AbstractDate;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="films")
@Entity
@Where(clause = "deleted_date is null")
public class Films extends AbstractDate implements Serializable{
    @Id
    @Column(name = "film_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @NotEmpty(message = "Film name is required!")
    @Column(name = "film_name")
    private String name;


    //    @NotEmpty(message = "Status is required!")
    @Column(name = "status")
    private Boolean status;

    @Column(name = "id_schedule")
    private Long idSchedule;

}
