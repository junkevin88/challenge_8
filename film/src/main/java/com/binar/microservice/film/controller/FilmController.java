package com.binar.microservice.film.controller;


import com.binar.microservice.film.entity.Films;
import com.binar.microservice.film.repository.FilmRepository;
import com.binar.microservice.film.service.FilmService;
import com.binar.microservice.film.utils.Response;
import com.binar.microservice.film.utils.SimpleStringUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Map;


@RestController
@RequestMapping("/film")
public class FilmController {

    SimpleStringUtils simpleStringUtils = new SimpleStringUtils();


    @Autowired
    public Response response;

    @Autowired
    public FilmRepository filmRepository;
    @Autowired
    private FilmService filmService;

    @PostMapping(value = {"/save", "/save/"})
//    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Map> save(@RequestBody Films films) {
//        if(errors.hasErrors()){
//            for (ObjectError error : errors.getAllErrors()) {
//                System.err.println(error.getDefaultMessage());
//            }
//            throw new RuntimeException("Validation Error");
//        }

        return new ResponseEntity<Map>(filmService.save(films), HttpStatus.OK);
    }


    @PutMapping("/update")
    public ResponseEntity<Map> update(@RequestBody Films films){
        return new ResponseEntity<Map>(filmService.update(films), HttpStatus.OK);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Map> getId(@PathVariable(value = "id") Long id){
        return new ResponseEntity<Map>(filmService.getById(id), HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
//    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Map> deleteById(@PathVariable(value = "id") Long id){
        return new ResponseEntity<Map>(filmService.delete(id), HttpStatus.OK);
    }

    @GetMapping("/status")
    public Iterable<Films> getFilmsFilterStatus(){
        return filmRepository.getFilmsFilterStatus();
    }

    @GetMapping("/list")
    public ResponseEntity<Map> listFilms(
            @RequestParam() Integer page,
            @RequestParam(required = true) Integer size,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String orderby,
            @RequestParam(required = false) String ordertype) {
        Pageable show_data = simpleStringUtils.getShort(orderby, ordertype, page, size);
        Page<Films> list = null;
        if(name != null && !name.isEmpty()){
            list = filmRepository.findByNameLike("%"+name+"%",show_data);
        }else {
            // nampilkan semuanya
            list = filmRepository.getListData(show_data);
        }
        return new ResponseEntity<Map>(response.templateSukses(list), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/message")
    public String test() {
        return "Test test jk 123...";
    }


    @GetMapping("/get/price/{id}")
    public ResponseEntity<Map> getScheduleId(@PathVariable(value = "id") Long id){
        return new ResponseEntity<Map>(filmService.getPriceInSchedule(id), HttpStatus.OK);
    }




    }
