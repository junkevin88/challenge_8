package com.binar.microservice.seat.controller;



import com.binar.microservice.seat.entity.Seats;
import com.binar.microservice.seat.repository.SeatRepository;
import com.binar.microservice.seat.service.SeatService;
import com.binar.microservice.seat.utils.Response;
import com.binar.microservice.seat.utils.SimpleStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/seat")
public class SeatController {
    @Autowired
    public Response response;

    @Autowired
    public SeatRepository seatRepository;


    @Autowired
    private SeatService seatService;

    SimpleStringUtils simpleStringUtils = new SimpleStringUtils();

    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<Map> save(@RequestBody Seats seats) {

        return new ResponseEntity<Map>(seatService.save(seats), HttpStatus.OK);
    }



    @PutMapping("/update")
    public ResponseEntity<Map> update(@RequestBody Seats seats){
        return new ResponseEntity<Map>(seatService.update(seats), HttpStatus.OK);
    }

//    @DeleteMapping("/delete")
//    public ResponseEntity<Map> deleteBySeatId(@RequestBody SeatId seatId){
//    public ResponseEntity<Map> deleteBySeatId(@RequestBody SeatId seatId){
//        return new ResponseEntity<Map>(seatService.delete(seatId), HttpStatus.OK);
//    }

    @GetMapping("/list")
    public ResponseEntity<Map> listSeats(
            @RequestParam() Integer page,
            @RequestParam(required = true) Integer size,
            @RequestParam(required = false) String orderby,
            @RequestParam(required = false) String ordertype) {
        Pageable show_data = simpleStringUtils.getShort(orderby, ordertype, page, size);
        Page<Seats> list = null;

            list = seatRepository.getListData(show_data);
        return new ResponseEntity<Map>(response.sukses(list), new HttpHeaders(), HttpStatus.OK);
    }

}
