package com.binar.microservice.seat.repository;

import com.binar.microservice.seat.entity.Seats;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SeatRepository extends JpaRepository<Seats, Long> {

    @Query(value = "select s from Seats s WHERE s.id = :idSeats", nativeQuery = false)
    public Seats getById(@Param("idSeats") Long id);

    @Query(value = "select s from Seats s ", nativeQuery = false)
    public Page<Seats> getListData(Pageable pageable);

//    @Query("FROM Users u WHERE LOWER(u.username) like LOWER(:username)")
//    public Page<Users> findByUsernameLike(String username, Pageable pageable);
//    public Page<Users> findByEmailLike(String  email, Pageable pageable);
//
//    public Page<Users> findByUsernameLikeAndEmailLike(String username, String  email, Pageable pageable);
//



}
