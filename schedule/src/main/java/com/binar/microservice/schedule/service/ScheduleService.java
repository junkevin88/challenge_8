package com.binar.microservice.schedule.service;

import com.binar.microservice.schedule.entity.Schedules;

import java.util.Map;

public interface ScheduleService {
    public Map save(Schedules request);

    public  Map update(Schedules request);

    public Map delete(Long id);

    public Map getById(Long request);


}
