package com.binar.microservice.schedule.utils;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@Component
public class Response {

    public Map templateSukses(Object objek){
        Map map = new HashMap();
        map.put("data", objek);
        map.put("message", "sukses");
        map.put("status", "200");
        return map;
    }

    public Map templateEror(Object objek){
        Map map = new HashMap();
        map.put("message", objek);
        map.put("status", "404");
        return map;
    }
    public Map notFound(Object objek){
        Map map = new HashMap();
        map.put("message", objek);
        map.put("status", "404");
        return map;
    }


    public boolean chekNull(Object obj){
        if(obj == null){
            return true;
        }
        return  false;
    }
    public Map Sukses(Object objek){
        Map map = new HashMap();
        map.put("message", "Success");
        map.put("status", "200");
        map.put("data", objek);

        return map;
    }

    public Map Error(Object objek){
        Map map = new HashMap();
        map.put("message", objek);
        map.put("status", "404");
        return map;
    }
    public boolean isValidEmail(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        return pat.matcher(email).matches();
    }
    public Map sukses(Object obj) {
        Map map = new HashMap();
        map.put("data", obj);
        map.put("code", 200);
        map.put("status", "Success!");
        return map;
    }

    public Map error(Object obj, Object code) {
        Map map = new HashMap();
        map.put("code", code);
        map.put("status", obj);
        return map;
    }
}