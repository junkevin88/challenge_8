package com.binar.microservice.schedule.controller;


import com.binar.microservice.schedule.entity.Schedules;
import com.binar.microservice.schedule.repository.ScheduleRepository;
import com.binar.microservice.schedule.service.ScheduleService;
import com.binar.microservice.schedule.utils.Response;
import com.binar.microservice.schedule.utils.SimpleStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/schedule")
public class ScheduleController {

    @Autowired
    public Response response;

    @Autowired
    public ScheduleRepository scheduleRepository;

    @Autowired
    private ScheduleService scheduleService;

    SimpleStringUtils simpleStringUtils = new SimpleStringUtils();


    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<Map> save(@RequestBody Schedules schedules) {
        return new ResponseEntity<Map>(scheduleService.save(schedules), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> deleteById(@PathVariable(value = "id") Long id){
        return new ResponseEntity<Map>(scheduleService.delete(id), HttpStatus.OK);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Map> getId(@PathVariable(value = "id") Long id){
        return new ResponseEntity<Map>(scheduleService.getById(id), HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@RequestBody Schedules schedules){
        return new ResponseEntity<Map>(scheduleService.update(schedules), HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<Map> listSchedules(
            @RequestParam() Integer page,
            @RequestParam(required = true) Integer size,
            @RequestParam(required = false) String orderby,
            @RequestParam(required = false) String ordertype) {
        Pageable show_data = simpleStringUtils.getShort(orderby, ordertype, page, size);
        Page<Schedules> list = null;
        list = scheduleRepository.getListData(show_data);
        return new ResponseEntity<Map>(response.sukses(list), new HttpHeaders(), HttpStatus.OK);
    }


    @GetMapping("/message")
    public String test() {
        return "Test test jk 123...";
    }

}
